import axios from 'axios'
import createPersistedState from 'vuex-persistedstate'

const getDefaultState = () => {
  return {
    token: '',
    user: {}
  }
}

export default {
  plugins: [createPersistedState()],
  namespaced: true,
  state: getDefaultState(),
  getters: {
    isLoggedIn: state => {
      // if (state.token) {
      //   return true
      // } else {
      //   return false
      // }
      return !!state.token
    },
    getUser: state => {
      return state.user
    }
  },
  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_USER: (state, user) => {
      state.user = user
    },
    RESET: state => {
      Object.assign(state, getDefaultState())
    }
  },
  actions: {
    login: ({ commit, dispatch }, { token, user }) => {
      commit('SET_TOKEN', token)
      commit('SET_USER', user)
      axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    },
    logout: ({ commit }) => {
      commit('RESET', '')
    }

  }

}
