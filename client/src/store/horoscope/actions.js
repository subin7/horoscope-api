/*
export function someAction (context) {
}
*/
import axios from 'axios'

export async function loadData ({ commit, state }, payload) {
  try {
    const url = `http://localhost:5000/api/signs?source=${state.query.source}&date=${state.query.date}`
    let { data } = await axios.get(url)
    const array = data.data.map((item, index) => item.name)
    commit('SET_APIDATA', { data, array })
  } catch (err) {
    commit('SET_APIDATA', { error: err.response.data.message })
  }
}
