/*
export function someMutation (state) {
}
*/
export function SET_APIDATA (state, payload) {
  state.apiData = payload.data
  state.signArray = payload.array
  state.error = payload.error
}

export function UPDATE_QUERY (state, query) {
  state.query = query
}
