const axios = require('axios')
const cron = require('node-cron')

const runCron = cron.schedule("* * * * *", async function () {
    console.log("Cron job initiated!")
    const response = await axios.get('http://localhost:5000/scrape/check')
    const data = response.data;
    console.log(data);
    });

module.exports = runCron