require("dotenv").config();
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const cors = require("cors");
const cron = require('./cron')
const PORT = process.env.PORT || 5000;

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.static("./public"));

const scrapeRouter = require("./routes/scrapeRouter.js");
app.use("/scrape", scrapeRouter);

const apiRouter = require("./routes/apiRouter.js");
app.use("/api/signs", apiRouter);

app.get("/", (req, res) => {
  res.sendFile("./public/index.html");
});

mongoose.connect(process.env.DATABASE_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
const db = mongoose.connection;
db.on("error", err => console.error(err));
db.once("open", () => console.log("DB Connected!"));

cron.runCron;

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
