const mongoose = require('mongoose')

const signSchema = new mongoose.Schema({
    date: {
        type: String,
        default: Date.now
    },
    data: {
        type: Array
    },
    source: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('Signs', signSchema)