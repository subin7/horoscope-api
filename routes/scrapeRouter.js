const express = require('express')
const router = express.Router()
const axios = require('axios')
const cheerio = require('cheerio')
const rp = require('request-promise')
const dateformat = require('dateformat')
const Signs = require('../models/Signs')
const userMiddleware = require('../middleware/users')

const now = new Date()
const date = dateformat(now, "yyyy-mm-dd")

router.get('/tkp', userMiddleware.isLoggedIn, async(req, res) => {
  const found = await Signs.findOne({ date, source: 'The Kathmandu Post' }, { _id: 0 })
  if(found) {
    return res.status(400).json({ message: 'Data is already scraped for today!' })
  }
  
  axios.get('https://kathmandupost.com/horoscope')
    .then(response => {
      let html = response.data
      let $ = cheerio.load(html)
      let signs = []
      let now = new Date()
      let date = dateformat(now, "yyyy-mm-dd")
      $(".block--horoscope").each((i, el) => {
        let sign = {
          name: $(el)
            .find(".col-sm-10 h5")
            .text()
            .trim(),
          horoscope: $(el)
            .find(".col-sm-10 p")
            .text().replace(/[*]+/g, '').trim()
        };
        signs.push(sign)
      });

      Signs.create({ data: signs, date: date, source: 'The Kathmandu Post' })
        .then(response => res.json(response))
    })
    .catch(err => {
      console.log(err);
    });
})

router.get('/tht', userMiddleware.isLoggedIn, async(req, res) => {
  // const found = Promise.resolve(Signs.findOne({ date, source: 'The Himalayan Times' }, { _id: 0 }))
  // found.then(value => {
  //   console.log(value)
  //   if(value) {
  //     return res.status(400).json({ message: 'Data is already scraped for today!' })
  //   }
  // })

  const found = await Signs.findOne({ date, source: 'The Himalayan Times' }, { _id: 0 })
  if(found) {
    return res.status(400).json({ message: 'Data is already scraped for today!' })
  }

  let $ = cheerio
  const horoParse = async (url) => {
    return rp(url)
      .then(html => {
        return {
          name: $("h2.catTitle", html).text().trim(),
          horoscope: $(".col-sm-9 > p", html).text().trim()
          // logo: $("div.wp-caption img", html).attr("src")
        };
      })
      .catch(err => {
        console.log(err)
      });
  };

  rp("https://thehimalayantimes.com/category/horoscopes/")
    .then(html => {
      const horoUrls = [];
      for (let i = 0; i < 12; i++) {
        horoUrls.push($("div.sign a", html)[i].attribs.href);
      }
      return Promise.all(
        horoUrls.map(url => {
          return horoParse(url);
        })
      );
    })
    .then(signs => {
      let now = new Date()
      let date = dateformat(now, "yyyy-mm-dd")
      Signs.create({ data: signs, date: date, source: 'The Himalayan Times' })
        .then(response => res.json(response))
    })
    .catch(err => {
      console.log(err);
    });

})

router.get('/check', (req, res) => {
  let now = new Date()
  res.send(dateformat(now, "dd-mm-yyyy hh:MM:ss"))
})

module.exports = router