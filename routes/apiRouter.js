const express = require('express')
const router = express.Router()
const dateformat = require('dateformat')
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken')
const Signs = require("../models/Signs");
const Users = require("../models/Users");
const userMiddleware = require('../middleware/users')

let now = new Date()
let date = dateformat(now, "yyyy-mm-dd")

router.get('/', async (req, res) => {
  try {
    if (!Object.keys(req.query).length) {
      return res.json({ message: 'Please pass at least one query parameter.' })
    }

    const dateQuery = req.query.date || date
    const sourceQuery = (req.query.source == "tkp") ? "The Kathmandu Post" : ((req.query.source == "tht") ? "The Himalayan Times" : "")
    if (!sourceQuery) {
      return res.status(400).json({ message: 'Please enter a valid source.' })
    }
    const signs = await Signs.findOne({ date: dateQuery, source: sourceQuery }, { _id: 0 })
    if (!signs) {
      res.status(400).json({ message: `Data for ${dateQuery} is unavailable.` })
    }
    else {
      res.json(signs)
    }
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})

router.get('/:name', async (req, res) => {
  try {
    if (!Object.keys(req.query).length) {
      return res.json({ message: 'Please pass at least one query parameter.' })
    }

    let requested = capitalize(req.params.name)
    let requestRegex = new RegExp("^" + requested, "g")
    const dateQuery = req.query.date || date
    const sourceQuery = (req.query.source == "tkp") ? "The Kathmandu Post" : ((req.query.source == "tht") ? "The Himalayan Times" : "")
    if (!sourceQuery) {
      return res.status(400).json({ message: 'Please enter a valid source.' })
    }
    sign = await Signs.findOne({ date: dateQuery, source: sourceQuery });
    if (!sign) {
      return res.status(400).json({ message: `Data for ${dateQuery} is unavailable.` })
    }

    let foundSign = sign.data.find(o => requestRegex.test(o.name))

    if (!foundSign) {
      return res.status(404).json({ message: "Sign not found!" });
    }
    else {
      return res.json(foundSign)
    }
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
})

router.post('/register', userMiddleware.isLoggedIn, async (req, res) => {
  const { userName, password } = req.body
  const user = await Users.findOne({ userName })

  if (req.userData) {
    return res.json(req.userData)
  }

  if (user) {
    return res.status(409).json({ message: `User with username ${userName} already exist!` })
  } else {
    bcrypt.hash(password, 10, (err, hash) => {
      if (err) {
        return res.status(500).json({ message: err.message })
      }
      Users.create({ userName, password: hash })
        .then(res.status(201).json({ message: `User with username ${userName} created!` }))
    })
  }

})

router.post('/login', async (req, res) => {
  const { userName, password } = req.body
  const user = await Users.findOne({ userName })

  if (!user) {
    return res.status(400).json({ message: 'Username or password is incorrect!' })
  }

  bcrypt.compare(password, user.password, (err, result) => {
    if (err) {
      return res.status(401).json({ message: err.message })
    }

    if (result) {
      const token = jwt.sign({
        username: user.userName,
        userId: user.id
      },
        process.env.SECRET, {
        expiresIn: '7d'
      });

      return res.status(200).send({
        message: 'Logged in!',
        token,
        user
      })
    }
    return res.status(401).send({
      message: 'Username or password is incorrect!'
    });
  })


})

const capitalize = s => {
  if (typeof s !== "string") return "";
  return s.charAt(0).toUpperCase() + s.slice(1);
};

module.exports = router